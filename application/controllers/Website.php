<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {

	public function index()
	{
        $data['title'] = 'Index';
        $this->load->view('website/templates/header', $data);
        $this->load->view('website/index');
        $this->load->view('website/templates/footer');

	}
    public function blog()
    {
        $data['title'] = 'Blog';
        $this->load->view('website/templates/header', $data);
        $this->load->view('website/blog');
        $this->load->view('website/templates/footer');
    }
    public function works()
    {
        $data['title'] = 'Works';
        $this->load->view('website/templates/header', $data);
        $this->load->view('website/works');
        $this->load->view('website/templates/footer');
    }
    public function worksdetail()
    {
        $data['title'] = 'Worksdetail';
        $this->load->view('website/templates/header', $data);
        $this->load->view('website/worksdetail');
        $this->load->view('website/templates/footer');
    }
}
