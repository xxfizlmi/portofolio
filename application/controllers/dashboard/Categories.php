<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{
        public function __construct()
        {
                parent::__construct();
                $this->load->model('M_categories');
                $this->load->library('form_validation');
        }
        public function index()
        {
                $data['title'] = 'Categories';
                $data['categories'] = $this->M_categories->getdataAll();
                $this->load->view('admin/templates/header', $data);
                $this->load->view('admin/templates/body');
                $this->load->view('admin/pages/categories/index', $data);
                $this->load->view('admin/templates/footer');
                $this->load->view('admin/templates/script');
        }
        public function add()
        {
                $categories = $this->M_categories;
                $validation = $this->form_validation->set_rules('name', 'Nama', 'Required');
                $name = $this->input->post('name');
                $data = array('name' => $name);
                if ($validation->run()) {
                        $categories->save($data);
                        $this->session->set_flashdata('success', 'Berhasil disimpan');
                }
                redirect('dashboard/categories');
        }
        public function edit($id = null)
        {
                $categories = $this->M_categories;
                $validation = $this->form_validation->set_rules('name', 'Nama', 'Required');
                $id = $this->input->post('id');
                $name = $this->input->post('name');
                $data = array(
                        'id'    => $id,
                        'name'  => $name);
                if ($validation->run()) {
                        $categories->update($data, array('id' => $id));
                        $this->session->set_flashdata('success', 'Berhasil disimpan');
                }
                redirect('dashboard/categories');
        }
        public function delete($id = null)
        {
                if ($this->M_categories->delete($id));
                $this->session->set_flashdata('success', 'Hapus ditelah data');
                redirect('dashboard/categories');
        }
}
