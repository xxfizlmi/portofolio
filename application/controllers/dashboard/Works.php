<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Works extends CI_Controller
{
        public function __construct()
        {
                parent::__construct();
                $this->load->model('M_works');
                $this->load->library('form_validation');
        }
        public function index()
        {
                $data['title'] = 'Works';
                $data['works'] = $this->M_works->getdataAll();
                $this->load->view('admin/templates/header', $data);
                $this->load->view('admin/templates/body');
                $this->load->view('admin/pages/works/index');
                $this->load->view('admin/templates/footer');
                $this->load->view('admin/templates/script');
        }
        public function add()
        {
                $works = $this->M_works;
                $validation = $this->form_validation->set_rules('title', 'Title', 'Required');
                $validation = $this->form_validation->set_rules('content', 'Content', 'Required');
                $validation = $this->form_validation->set_rules('featured_image', 'Featured Image', 'Required');
                // $id = $this->input->uniqid();
                $title = $this->input->post('title');
                $content = $this->input->post('content');
                $featured_image = $_FILES['featured_image'];
                if ($featured_image = "") {
                } else {
                        $config['upload_path']      = './uploads';
                        $config['allowed_types']    = 'gif|jpg|png|jpeg';
                        $config['max_size']         = 100;
                        $config['max_width']        = 1024;
                        $config['max_height']       = 768;
                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('featured_image')) {
                                $error = array('error' => $this->upload->display_errors());
                        } else {
                                $featured_image = $this->upload->data('file_name');
                        }
                }

                $data = array(
                        // 'id'    => $id,
                        'title' => $title,
                        'content' => $content,
                        'featured_image' => $featured_image
                );
                if ($validation->run()) {
                        $works->save($data);
                        $this->session->set_flashdata('success', 'Berhasil disimpan');
                }
                redirect('dashboard/works');
        }
        public function edit()
        {
                $works = $this->M_works;
                $validation = $this->form_validation->set_rules('title', 'Title', 'Required');
                $validation = $this->form_validation->set_rules('content', 'Content', 'Required');
                $validation = $this->form_validation->set_rules('featured_image', 'Featured Image', 'Required');
                $id = $this->input->post('id');
                $title = $this->input->post('title');
                $content = $this->input->post('content');
                $featured_image = $_FILES['featured_image'];
                if ($featured_image = "") {
                } else {
                        $config['upload_path']      = './uploads';
                        $config['allowed_types']    = 'gif|jpg|png|jpeg';
                        $config['max_size']         = 100;
                        $config['max_width']        = 1024;
                        $config['max_height']       = 768;
                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('featured_image')) {
                                $error = array('error' => $this->upload->display_errors());
                        } else {
                                $featured_image = $this->upload->data('file_name');
                        }
                }

                $data = array(
                        'id' => $id,
                        'title' => $title,
                        'content' => $content,
                        'featured_image' => $featured_image
                );
                if ($validation->run()) {
                        $works->update($data, array('id' => $id));
                        $this->session->set_flashdata('success', 'Berhasil disimpan');
                }
                redirect('dashboard/works');
        }
        public function delete($id=null)
        {
                if ($this->M_works->delete($id));
                $this->session->set_flashdata('success', 'Hapus ditelah data');
                redirect('dashboard/works');
        }
}
