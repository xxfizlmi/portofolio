      <!-- footer -->
      <footer class="container mt-5 py-5 text-center">
          <div class="d-flex justify-content-center">
              <a href="#"><img src="<?php echo base_url('assets/website/img/fb.png');?>" alt="facebook" class="img-fluid"></a>
              <a href="#"><img src="<?php echo base_url('assets/website/img/insta.png');?>" alt="instagram" class="img-fluid"></a>
              <a href="#"><img src="<?php echo base_url('assets/website/img/twitter.png');?>" alt="twitter" class="img-fluid"></a>
              <a href="#"><img src="<?php echo base_url('assets/website/img/Linkedin.png');?>" alt="Linkedin" class="img-fluid"></a>
          </div>
          <p class="my-4">Copyright ©2020 All rights reserved </p>
      </footer>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
  </body>
</html>