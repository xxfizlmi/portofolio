<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- own css -->
    <link rel="stylesheet" href="<?= base_url('assets/website/style.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/website/blogStyle.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/website/worksdetail.css');?>">

    <!-- icon -->
    <script src="https://kit.fontawesome.com/244c1245b5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://fonts.google.com/specimen/Big+Shoulders+Display">
    <link rel="icon" href="<?= base_url('assets/website/img/icon.png');?>">
    <title><?= $title ?></title>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container-fluid justify-content-end">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarScroll">
            <div class="navbar-nav text-end">
              <li class="nav-item dropdown justify-content-end">
                <a class="blog" data-spy="scroll" style="padding-right: 30px" href="<?= site_url('blog');?>">Blog</a>
              </li>
              <li class="nav-item dropdown">
                <a class="works" data-spy="scroll" style="padding-right: 30px" href="<?= site_url('works');?>">Works</a>
              </li>
              <li class="nav-item dropdown">
                <a class="contact" data-spy="scroll" style="padding-right: 30px" href="<?= site_url('worksdetail');?>">Contact</a>
              </li>
              <li class="nav-item dropdown">
                <a class="contact" data-spy="scroll" style="padding-right: 30px" href="<?= site_url('dashboard/dashboard');?>">Admin</a>
              </li>
            </div>
          </div>
        </div>
      </nav>