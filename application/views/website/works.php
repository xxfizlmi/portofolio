    <div class="row1">    
    <!-- featured -->
      <section class="featured">
        <div class="container">
            <div class="row">
              <h1>Work</h1>
                <div class="featuredText my-4 text-md-start text-center"></div>
                <div class="baris d-md-flex">
                  <img src="<?php echo base_url('assets/website/img/Rectangle 30.png');?>" alt="fitur 1" class="img-fluid">
                    <div class="text mt-md-0 mt-4 px-4">
                        <h2><a href="worksdetail.html">Designing Dashboards</a></h2>
                        <div class="d-flex my-4">
                            <div class="tahun me-4">2020</div>
                            <div class="keterangan">Dashboard</div>
                        </div>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <hr class="my-5">
                <div class="baris d-md-flex">
                    <img src="<?php echo base_url('assets/website/img/Rectangle 32.png');?>" alt="fitur 2" class="img-fluid">
                    <div class="text mt-md-0 mt-4 px-4">
                        <h2>Vibrant Portraits of 2020</h2>
                        <div class="d-flex my-4">
                            <div class="tahun me-4">2018</div>
                            <div class="keterangan">Illustration</div>
                        </div>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <hr class="my-5">
                <div class="baris d-md-flex">
                    <img src="<?php echo base_url('assets/website/img/Rectangle 34.png');?>" alt="fitur 3" class="img-fluid">
                    <div class="text mt-md-0 mt-4 px-4">
                        <h2>36 Days of Malayalam type</h2>
                        <div class="d-flex my-4">
                            <div class="tahun me-4">2018</div>
                            <div class="keterangan">Typography</div>
                        </div>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <hr class="my-5">
                <div class="baris d-md-flex">
                  <img src="<?php echo base_url('assets/website/img/Rectangle 40.png');?>" alt="fitur 3" class="img-fluid">
                  <div class="text mt-md-0 mt-4 px-4">
                      <h2>Components</h2>
                      <div class="d-flex my-4">
                          <div class="tahun me-4">2018</div>
                          <div class="keterangan">Components, Design</div>
                      </div>
                      <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                  </div>
              </div>
              <hr class="my-5">
            </div>
        </div>
    </section>