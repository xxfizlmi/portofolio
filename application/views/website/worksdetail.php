     <!-- section design -->
    <section id ="worksdetail ">
        <div class="container">
          <h1 class="fw-bold-md">Designing Dashboards with usability in mind</h1>
          <div class="d-flex">
            <h3 style="color: white;margin-top: 7px; ">2020</h3>
            <div class="sub-title-md ms-3" style="
            font-size: 20px;;">Dashboard, User Experience Design</div>
          </div>
        <div class="justify-content-center">
          <p style="position: center; color: rgb(0, 0, 0);font-size: 16px;padding-bottom: 5px;padding-top: 10px;">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
          <img class="img-fluid text-center" src="<?php echo base_url('assets/website/img/img1.png');?>">
        </div>
        <div class="heading">
          <h1>Heading 1</h1>
          <h2>Heading 2</h2>
          <p style="position: center; color: rgb(0, 0, 0);font-size: 16px;padding-bottom: 5px;padding-top: 10px;">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
        </div>
        <div>     
          <img class="img-fluid" src="<?php echo base_url('assets/website/img/img2.png');?>">
          <img class="img-fluid" src="<?php echo base_url('assets/website/img/img3.png');?>">
        </div>
      </div>
    </section>