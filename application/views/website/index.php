      <!-- hero -->
      <section class="hero">
          <div class="container mb-5">
              <div class="row flex-column-reverse flex-md-row text-md-start text-center mb-5">
                  <div class="col-md-6 mt-5 mt-md-0">
                    <h1>Hi, I am John,</h1>
                    <h1>Creative Technologist</h1>
                    <p class="mt-md-5 mt-4">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    <a href="#" class="btn mt-md-5 mt-4">Download Resume</a>
                  </div>
                  <div class="col-md-5 text-md-end ">
                      <img src="<?php echo base_url('assets/website/img/profil.png')?>" class="img-fluid  mt-5 mt-md-0" alt="">
                  </div>
              </div>
          </div>
      </section>

      <!-- recent post -->
      <section class="recent">
          <div class="container p-4">
              <div class="row mb-3">
                  <div class="col d-flex justify-content-md-between justify-content-center">
                    <div class="RecentText text-md-start text-center">Recent posts</div>
                    <a href="#" class="viewText">view all</a>
                  </div>
              </div>
              <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2 g-4">
                <div class="col">
                    <div class="card bg-white rounded-3 py-4 px-md-5 px-3 border-0">
                        <h3>Making a design system from scratch</h3>
                        <div class="d-flex mt-4">
                            <p>12 Feb 2020</p>
                            <p class="ms-3 me-3">|</p>
                            <p class="keterangan">Design, Pattern</p>
                        </div>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-white rounded-3 py-4 px-md-5 px-3 border-0">
                        <h3>Creating pixel perfect icons in Figma</h3>
                        <div class="d-flex mt-4">
                            <p>12 Feb 2020</p>
                            <p class="ms-3 me-3">|</p>
                            <p>Figma, Icon Design</p>
                        </div>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
              </div>
              </div>
          </div>
      </section>

      <!-- featured -->
      <section class="featured">
          <div class="container">
              <div class="row">
                  <div class="featuredText my-4 text-md-start text-center">Featured works</div>
                  <div class="baris d-md-flex">
                    <img src="<?php echo base_url('assets/website/img/fitur 1.png');?>" alt="fitur 1" class="img-fluid">
                      <div class="text mt-md-0 mt-4 px-4">
                          <h2><a href="worksdetail.html">Designing Dashboards</a></h2>
                          <div class="d-flex my-4">
                              <div class="tahun me-4">2020</div>
                              <div class="keterangan">Dashboard</div>
                          </div>
                          <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                      </div>
                  </div>
                  <hr class="my-3">
                  <div class="baris d-md-flex">
                      <img src="<?php echo base_url('assets/website/img/fitur 2.png');?>" alt="fitur 2" class="img-fluid">
                      <div class="text mt-md-0 mt-4 px-4">
                          <h2>Vibrant Portraits of 2020</h2>
                          <div class="d-flex my-4">
                              <div class="tahun me-4">2018</div>
                              <div class="keterangan">Illustration</div>
                          </div>
                          <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                      </div>
                  </div>
                  <hr class="my-3">
                  <div class="baris d-md-flex">
                      <img src="<?php echo base_url('assets/website/img/fitur 3.png');?>" alt="fitur 3" class="img-fluid">
                      <div class="text mt-md-0 mt-4 px-4">
                          <h2>36 Days of Malayalam type</h2>
                          <div class="d-flex my-4">
                              <div class="tahun me-4">2018</div>
                              <div class="keterangan">Typography</div>
                          </div>
                          <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                      </div>
                  </div>
                  <hr class="my-3">
              </div>
          </div>
      </section>