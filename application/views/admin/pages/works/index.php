  <div class="container-fluid py-4">
    <div class="row">
      <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6><?= $title; ?></h6>
          </div>
          <?php if ($this->session->flashdata('success')) : ?>
              <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
              </div>
            <?php endif; ?>
          <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
              <div class="card-header pb-0">
                <a href="posts/add" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#wAdd"><i class="fas fa-plus" aria-hidden="true"></i>
                  Add
                </a>
              </div>
            </div>
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Title</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kategori</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Content</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">FeaturedImage</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Created At</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Updated At</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Created by</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Updated by</th>
                  <th class="text-secondary opacity-7">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($works as $work) : ;?>
                  <tr>
                    <td>
                      <div class="d-flex px-2 py-1">
                        <div class="d-flex flex-column justify-content-center">
                          <h6 class="mb-0 text-sm"><?= $work->id;?></h6>
                        </div>
                      </div>
                    </td>
                    <td>
                      <p class="text-xs font-weight-bold mb-0"><?= $work->title;?></p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success">Online</span>
                    </td>
                    <td>
                      <p class="text-xs font-weight-bold mb-0"><?= $work->content;?></p>
                    </td>
                    <td class="align-middle text-center">
                      <img src="<?php echo base_url('uploads/' . $work->featured_image);?>" class="avatar avatar-sm me-3" alt="user1">
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success"><?= $work->created_at;?></span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success"><?= $work->updated_at;?></span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success"></span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success"></span>
                    </td>
                    <td class="align-middle">
                      <a class="btn btn-primary" href="#" role="button" data-bs-toggle="modal" data-bs-target="#wEdit"><i class="fas fa-edit"></i>  Edit</a>
                      <a class="btn btn-primary" href="#" role="button" data-bs-toggle="modal" data-bs-target="#wHapus"><i class="fas fa-trash"></i>  Hapus</a>
                    </td>
                  </tr>
                  <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Add Form -->
  <div class="modal fade" id="wAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Buat Works</h5>
        </div>
        <div class="modal-body">
          <form action="<?= site_url('dashboard/works/add');?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name='title' class="form-control" id="title" aria-describedby="title" placeholder="Title">
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="content">Kategori</label>
              <select class="form-select form-select-sm" aria-label=".form-select-sm example">
              <option selected>Open this select menu</option>
                <option name="categories_id" value=""></option>
              </select>
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="content">Content</label>
              <input type="text" name='content' class="form-control" id="content" aria-describedby="content" placeholder="Content">
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="featured_image">Featured Image</label>
              <input type="file" name="featured_image" class="form-control" id="featured_image" placeholder="featured_image">
              <div class="invalid-feedback"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i>  Batal</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-save" aria-hidden="true"></i>  Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End Add Form -->
  <!-- Edit Form -->
  <div class="modal fade" id="wEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ubah Works</h5>
        </div>
        <div class="modal-body">
          <form action="<?= site_url('dashboard/works/edit') ;?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name='title' class="form-control" id="title" aria-describedby="title" placeholder="Title" value="">
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="content">Kategori</label>
              <select class="form-select form-select-sm" aria-label=".form-select-sm example"> 
              <option selected>Open this select menu</option>
                <option value="">Select</option>
              </select>
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="content">Content</label>
              <input type="text" name='content' class="form-control" id="content" aria-describedby="content" placeholder="Content" value="">
              <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
              <label for="featured_image">Featured Image</label>
              <img src="">
              <input type="file" name="featured_image" class="form-control" id="featured_image" placeholder="featured_image" value="">
              <div class="invalid-feedback"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i>  Batal</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-save" aria-hidden="true"></i>  Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End Edit Form -->
  <!-- Hapus Form Modal -->
  <div class="modal fade" id="wHapus" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
      </div>
      <div class="modal-body">
        Apakah kamu yakin?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i>Batal</button>
        <a class="btn btn-primary" href="<?= site_url('dashboard/works/delete/' . $work->id);?>" role="button"><i class="fas fa-trash"></i>Hapus</a>
      </div>
    </div>
  </div>
</div>
<!-- End Hapus Form Modal -->