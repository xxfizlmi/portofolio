    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6><?= $title; ?></h6>
            </div>
            <?php if ($this->session->flashdata('success')) : ?>
              <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
              </div>
            <?php endif; ?>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <div class="card-header pb-0">
                  <a href="posts/add" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#cAdd"><i class="fas fa-plus" aria-hidden="true"></i>
                    Add
                  </a>
                </div>
              </div>
            </div>
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nama</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Created At</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">updated At</th>
                  <th class="text-secondary opacity-7">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($categories as $category) :; ?>
                  <tr>
                    <td>
                      <div class="d-flex px-2 py-1">
                        <div class="d-flex flex-column justify-content-center">
                          <h6 class="mb-5 text-sm-center"><?= $category->id; ?></h6>
                        </div>
                      </div>
                    </td>
                    <td>
                      <p class="text-xs-center font-weight-bold mb-5"><?= $category->name; ?></p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm-center bg-gradient-success mb-5"><?= $category->created_at; ?></span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="badge badge-sm bg-gradient-success"><?= $category->updated_at; ?></span>
                    </td>
                    <td class="align-middle">
                      <a class="btn btn-primary" href="#" role="button" data-bs-toggle="modal" data-bs-target="#cEdit<?= $category->id;?>"><i class="fas fa-edit" aria-hidden="true"></i> Edit</a>
                      <a class="btn btn-primary" href="#" role="button" data-bs-toggle="modal" data-bs-target="#cHapus<?= $category->id;?>"><i class="fas fa-trash" aria-hidden="true"></i> Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Add Form -->
    <div class="modal fade" id="cAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Buat Kategori</h5>
          </div>
          <div class="modal-body">
            <form action="<?= site_url('dashboard/categories/add'); ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name='name' class="form-control" id="name" aria-describedby="emailHelp" placeholder="Name">
                <div class=""></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i> Batal</button>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save" aria-hidden="true"></i> Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Add Form -->
    <!-- Edit Form -->
    <div class="modal fade" id="cEdit<?= $category->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ubah Posts</h5>
          </div>
          <div class="modal-body">
            <form action="<?= site_url('dashboard/categories/edit'); ?>" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name='name' class="form-control" id="name" aria-describedby="name" placeholder="Name" value="<?= $category->name;?>">
                <div class=""></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i> Batal</button>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save" aria-hidden="true"></i> Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Edit Form -->
    <!-- Hapus Form Modal -->
    <div class="modal fade" id="cHapus<?= $category->id;?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Hapus Kategori</h5>
          </div>
          <div class="modal-body">
            Apakah kamu yakin?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i> Batal</button>
            <a class="btn btn-primary" href="<?= site_url('dashboard/categories/delete/' . $category->id);?>" role="button"><i class="fas fa-trash"></i> Hapus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- End Hapus Form Modal -->