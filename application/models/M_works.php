<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_works extends CI_Model {

	public function getdataAll()
	{
        $this->db->select('*');
        $this->db->from('works');
        $query = $this->db->get();
        return $query->result();
    }
    public function save($data)
    {
        $query = $this->db->insert('works',$data);
        return $query;
    }
    public function update($data, $id)
    {
        return $this->db->update('works', $data, $id);
    }
    public function delete($id){
        $this->db->delete('works', array("id" =>$id));
    }
}
