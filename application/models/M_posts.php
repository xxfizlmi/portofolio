<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_posts extends CI_Model {

	public function getdataAll()
	{
        $this->db->select('*');
        $this->db->from('posts');
        $query = $this->db->get();
        return $query->result();
    }
    public function save($data)
    {
        $query = $this->db->insert('posts',$data);
        return $query;
    }
    public function update($data, $id)
    {
        return $this->db->update('posts', $data, $id);
    }
    public function delete($id)
    {
        return $this->db->delete('posts', array("id" => $id));
    }
}
