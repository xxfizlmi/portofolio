<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_categories extends CI_Model {

	public function getdataAll()
	{
        $this->db->select('*');
        $this->db->from('categories');
        $query = $this->db->get();
        return $query->result();
    }
    public function save($data)
    {
        $query = $this->db->insert('categories',$data);
        return $query;
    }
    function update($data, $id)
    {
        return $this->db->update('categories', $id, $data);
    }
    public function delete($id){
        $this->db->delete('categories', array("id" =>$id));
    }
}
